<footer>
    <div class="wrap">
        <!-- <ul class="nav"> -->
        <!-- <li><a href="#">Меню</a></li> -->
        <!-- <li><a href="#">Оплата и доставка</a></li> -->
        <!-- <li><a href="#">Отзывы</a></li> -->
        <!-- <li><a href="#">События</a></li> -->
        <!-- <li><a href="#">Галерея</a></li> -->
        <!-- <li><a href="#">О нас</a></li> -->
        <!-- <li><a href="#">Контакты</a></li> -->
        <!-- </ul> -->
        <div class="row">
            <div class="col-6">
                <h2 style="margin-top: 0;" id="contacts">Контакты</h2>
                <div class="contacts">
                    <div><i class="icon-location"></i>ул. Шаабдан батыр, n/n</div>
                    <div><i class="icon-email"></i><a href="mailto:info@baibol.kg">info@baibol.kg</a></div>
                    <div><i class="icon-phone"></i><a href="tel:+996770780808">+996 312 888008</a></div>
                    <div><i class="icon-mobile"></i><a href="tel:+996770780808">+996 770 780808</a></div>
                    <div class="networks">
                        <a target="_blank" href="https://www.facebook.com/baibolresto" title="Facebook"><span class="icon-brand17"></span></a>
                        <a target="_blank" href="https://www.instagram.com/baibolresto" title="Подписаться в Instagram"><span class="icon-brand20"></span></a>
                        <a target="_blank" href="https://www.tripadvisor.ru/Restaurant_Review-g293948-d12460821-Reviews-Baibol_Ethnic_Restaurant-Bishkek.html" title="Оставить отзыв на Tripadvisor"><span class="icon-brand23"></span></a>
                    </div>
                    <div style="font-size: 8pt;">Все права защишены © BAIBOL.KG, 2017</div>
                </div>
            </div>
            <div class="col-6">
                <div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d939.6957648024085!2d74.64308529824201!3d42.82097977204546!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x389eb42c590da875%3A0x3bc551a0130a4902!2zNDLCsDQ5JzE2LjIiTiA3NMKwMzgnMzUuNiJF!5e0!3m2!1sru!2sru!4v1508441039831" width="100%" height="320" frameborder="0" style="border:0" allowfullscreen></iframe></div>
            </div>
        </div>
    </div>
</footer>