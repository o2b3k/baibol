<!DOCTYPE html>
<html>
<head>
    <title>Таш-Кордо - древнее блюдо кыргызов.</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Мясо из ягненка, баранины. Заказать онлайн Таш-Кордо. Бишкек - Кыргызстан. Baibol Ethno Restaurant." />
    <meta name="keywords" content="" />

    <link rel="icon" type="image/jpeg" href="{{ asset('images/favicon.jpg') }}" />

    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('lib/swiper/dist/css/swiper.min.css') }}" />
    <script src="{{ asset('lib/jquery-3.2.1.min.js') }}"></script>
    <!-- Include Required Prerequisites -->
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="{{ asset('lib/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('lib/daterangepicker/daterangepicker.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/daterangepicker/daterangepicker.css') }}" />

</head>
<body>
    @include('layouts.nav')
    @include('layouts.header')
    @include('layouts.content')
    @include('layouts.footer')
</body>
<script src="{{ asset('lib/fancybox/dist/jquery.fancybox.min.js') }}"></script>
<script src="{{ asset('lib/swiper/dist/js/swiper.min.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
<html>