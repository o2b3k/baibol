<header>
    <!-- <div class="banner"> -->
    <!-- <div class="swiper-wrapper"> -->
    <!-- <div class="swiper-slide" style="background: url(http://resto1.baibol.kg/images/header.jpg);">Slide 1</div> -->
    <!-- </div> -->
    <!-- </div> -->
    <style>
        @import url('https://fonts.googleapis.com/css?family=El+Messiri:400,500,600,700|Open+Sans:400,600,700,800|Pangolin|Playfair+Display:400,700,900');
        header h1 {
            position: absolute;
            bottom: 0;
            z-index: 2;
            color: #d4bd9b;
            margin: 1em 0;
            font-size: 36pt;
            line-height: 1em;
            text-shadow: 0 1px 2px rgba(0,0,0,.75);
            font-family: 'El Messiri', sans-serif;
            font-weight: 700
        }
        header h1 > * {
            font-family: 'El Messiri', sans-serif;
            font-weight: 700
        }
        .transparent {
            position: absolute;
            top: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            background: rgba(0,0,0,.2);
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0.9), rgba(0, 0, 0, 0.1));
            z-index: 1;
        }

        .video-container {
            position: absolute;
            top: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            overflow: hidden;
            z-index: 0;
        //animation: animationFrames 5s infinite ease;
        }

        @keyframes animationFrames{
            50% {
                filter: blur(20px);
            }
        }


        .video-container video {
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%,-50%);
        }
    </style>
    <div class="wrap"><h1><span style="color:#fff;">Таш-Кордо</span> <br>древнее блюдо кыргызов</h1></div>
    <div class="transparent"></div>
    <div class="video-container">
        <!--<img src="/images/discount.png">-->
        <video id="video" autoplay="autoplay" loop="loop" preload="auto">
            <source src="{{ asset('video/tashkordo.mp4') }}"></source>
        </video>
    </div>
</header>