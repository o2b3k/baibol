<nav id="nav">
    <!-- <div class="bg"></div> -->
    <div class="wrap row after" style="position: relative; z-index: 1; padding: 0 1em; box-sizing: border-box;">
        <div class="col-4">
            <a href="#" id="to_order" style="padding: 8px; margin: 12px 12px 12px 0; border-radius: 3px; line-height: 18px; background: #4CAF50;
    color: #fff;" class="link">Заказать Таш-Кордо</a>
            <!-- <a href="/gallery/" class="link">Галерея</a> -->
            <!-- <a href="/delivery/" class="link">Доставка и оплата</a> -->
        </div>
        <div class="col-4 center">
            <div class="circle"><a href="/" class="logo" title="Baibol Resto">Baibol Resto</a></div>
        </div>
        <div class="col-4 right">
            <div id="phone" style="margin: 10px 20px 10px 0; line-height: 40px; float:left; margin-right:1.5em; font-size:22px;">
                <div style="float:left;">0</div>
                <div style="margin:10px 0; float:left; font-weight:bold; padding:0 5px;">
                    <div style="line-height:10px; font-size:10px;">505</div>
                    <div style="line-height:10px; font-size:10px;">312</div>
                </div>
                <div style="float:left;">888008</div>
            </div>
            <a href="#contacts" style="float:left;" class="link">Контакты</a>
            <div class="clear"></div>
        </div>
    </div>
</nav>
<style>
    nav {
        background: transparent;
        transition: .5s;
    }
    .site-scroll--active nav {
        background: #211c1a;
        transition: .5s;
    }
    .circle{
        width: 120px;
        height: 110px;
        margin: 0 auto;
        border-radius: 100%;
        position: relative;
        background: transparent;
        transition: .5s;
    }
    .site-scroll--active .circle{
    <!-- background: #211c1a; -->
        transition: .5s;
        height: 60px;
    }
</style>
<script>
    $(function(){

        var header_offset = $('html').offset().top + 0;

        function slate_site_scrolled() {
            var y_scroll_pos = window.pageYOffset;

            if(y_scroll_pos > header_offset) {
                $('html').removeClass('site-scroll--inactive');
                $('html').addClass('site-scroll--active');
            }else {
                $('html').removeClass('site-scroll--active');
                $('html').addClass('site-scroll--inactive');
            }
        }

        slate_site_scrolled();

        $(window).on('scroll', function() {
            slate_site_scrolled();
        });

    });
</script>