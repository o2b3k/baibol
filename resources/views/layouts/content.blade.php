<style>
    .imgs{
        width:289.3px;
        margin:0 30px 30px 0;
        box-sizing:border-box;
        float:left;
    }
</style>
<div class="wrap">
    <div class="content tashkordo">
        <div style="width:700px; margin:0 auto; padding:2em 0;"><p style="text-align:center; font-size:18px; font-weight:bold;" >Закажите Таш-Кордо – национальное блюдо кыргызского народа. Наш этно-ресторан «Байбол» приготовит его по подлинному рецепту и доставит прямо к праздничному столу. Ваши гости непременно оценят лучшее блюдо охотников. От изумительного вкуса Таш-Кордо останутся в восторге даже самые искушенные гурманы. Оформите заказ прямо сейчас! </p></div>
        <div style="text-align: center;">
            <a class="figure" data-fancybox="gallery" href="/images/img2.JPG" data-caption=""><img src="/images/img2.JPG" class="imgs"></a>
            <a class="figure" data-fancybox="gallery" href="/images/img13.jpg" data-caption=""><img src="/images/img13.jpg" class="imgs"></a>
            <a class="figure" data-fancybox="gallery" href="/images/img3.JPG" data-caption=""><img src="/images/img3.JPG" class="imgs" style="margin-right:0;"></a>

            <div class="clear"></div>
            <button class="btn toorder" style="background:#4CAF50; color:#fff;">Заказать Таш-Кордо</button>

        </div>
        <br />
        <div class="col-6" style="padding-right: 1em;"><p style="text-align: justify;">
                Таш-Кордо – излюбленное блюдо древних кыргызских охотников. Его особенность заключается в том, что в процессе приготовления не используется посуда. По древней технологии охотники сначала выкапывали яму, глубиной и шириной около 1 метра. Потом расставляли по ее стенкам камни и раскладывали дрова. Когда оставались только раскаленные угли, над ними подвешивали тушу животного, которую удалось добыть на охоте. Мясо предварительно мариновалось в соли и чесноке за сутки до его приготовления. «Печь» обязательно закрывалась ветками и шкурой и засыпалось землей, чтобы горячий пар из углубления не выходил наружу.
            </p></div>
        <div class="col-6" style="padding-left: 1em;"><p style="text-align: justify;">Этно-ресторан «Байбол» возрождает древние национальные традиции! Сегодня каждый желающий может заказать Таш-Кордо в Бишкеке. Наши повара приготовят такое блюдо из туши молодого барашка традиционным способом по старинному рецепту, а также позаботятся о вкусных гарнирах и соусах к нему. Вы и Ваши гости будете приятно удивлены восхитительным вкусом мяса, который не подается сравнению ни с каким жаренным мясом современных кухонь. Вот уже более 10 лет мы радуем этим блюдом всех ценителей национальной кухни и знаем все тонкости его приготовления. Закажите Таш-Кордо в Бишкеке прямо сейчас!</p></div>
        <div class="clear"></div>
    </div>
</div>
<div class="wrap">
    <div class="content">
        <h2>Отзывы</h2>
        <div class="row reviews">
            <div class="col-4 review">
                <div class="name">Атай</div>
                <div class="comment">Я не самый большой знаток кыргызской кухни, но то, что я сегодня попробовал и услышал про Таш-кордо - это целая философия! Вкус и качество на самом высоком уровне. Здесь действительно с душой и трепетом относятся к своему делу. Спасибо Мансуру, за приглашение! Достаточно.</div>
            </div>
            <div class="col-4 review">
                <div class="name">Карл</div>
                <div class="comment">Определенно один из самых лучших "этнических" ресторанов в Бишкеке. Хорошая еда, обслуживание и атмосфера. Я особенно оценил тематический номер со старыми фотографиями из истории Кыргызстана. Это место обязательно для кочевых любителей культуры!</div>
            </div>
            <div class="col-4 review">
                <div class="name">Аман</div>
                <div class="comment">Упаковка впечатляет! Но содержимое оказалось выше всех ожиданий! Чего стоит желтая морковь и хлопковое масло! Даже специально руки не стал мыть чтобы дольше сохранить ароматы далекого детства Так держать!</div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="content">
        <h2>Доставка и оплата</h2>
        <div class="row deliverypayment">
            <div class="col-4 row">
                <div class="col-3">
                    <img src="http://resto.baibol.kg/images/telemarketer.png" />
                </div>
                <div class="col-9">
                    <h3>Заказ</h3>
                    <p>Заказ оформляется за сутки до осуществления доставки.</p>
                </div>
            </div>
            <div class="col-4 row">
                <div class="col-3">
                    <img src="http://resto.baibol.kg/images/wallet.png" />
                </div>
                <div class="col-9">
                    <h3>Оплата</h3>
                    <p>Предоплата в размере 50% от стоимости заказа следующими способами: онлайн, в терминалах и наличными.</p>
                </div>
            </div>
            <div class="col-4 row">
                <div class="col-3">
                    <img src="http://resto.baibol.kg/images/truck.png" />
                </div>
                <div class="col-9">
                    <h3>Доставка</h3>
                    <p>По городу Бишкек - бесплатно!</p>
                </div>
            </div>
        </div>
        <div id="more" style="color: #795548; text-decoration: underline; float:right; margin-bottom:1em;">Подробнее</div>
        <div class="clear"></div>
    </div>
</div>


<!-- <div class="wrap"> -->
<!-- <div class="content"> -->
<!-- <h2>Оставайтесь с нами на связи</h2> -->
<!-- <div class="row networks"> -->
<!-- <div class="col-4 row"> -->
<!-- <div class="col-3" align="center"> -->
<!-- <a href="https://www.facebook.com/baibolresto" title="Facebook"><i class="icon-brand17" style="font-size: 36pt"></i></a> -->
<!-- </div> -->
<!-- <div class="col-9"> -->
<!-- <a href="https://www.facebook.com/baibolresto"><h3>Facebook</h3></a> -->
<!-- <p>Будьте в курсе всех событий на нашей странице</p> -->
<!-- </div> -->
<!-- </div> -->
<!-- <div class="col-4 row"> -->
<!-- <div class="col-3" align="center"> -->
<!-- <a href="https://www.tripadvisor.ru/Restaurant_Review-g293948-d12460821-Reviews-Baibol_Ethnic_Restaurant-Bishkek.html" title="Оставить отзыв на Tripadvisor"><i class="icon-brand23" style="font-size: 36pt"></i></a> -->
<!-- </div> -->
<!-- <div class="col-9"> -->
<!-- <a href="https://www.tripadvisor.ru/Restaurant_Review-g293948-d12460821-Reviews-Baibol_Ethnic_Restaurant-Bishkek.html"><h3>Tripadvisor</h3></a> -->
<!-- <p>Оставляйте отзыв</p> -->
<!-- </div> -->
<!-- </div> -->
<!-- <div class="col-4 row"> -->
<!-- <div class="col-3" align="center"> -->
<!-- <a href="https://www.facebook.com/baibolresto" title="Подписаться в Instagram"><i class="icon-brand20" style="font-size: 36pt"></i></a> -->
<!-- </div> -->
<!-- <div class="col-9"> -->
<!-- <a href="https://www.facebook.com/baibolresto"><h3>Instagram</h3></a> -->
<!-- <p>Подпишитесь на нас в Instagram!</p> -->
<!-- </div> -->
<!-- </div> -->
<!-- </div> -->
<!-- </div> -->
<!-- </div> -->