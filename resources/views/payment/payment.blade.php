@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Payment</div>

                    <div class="panel-body">
                        <form action="{{ route('payment.Payment') }}" method="get">
                            {{ csrf_field() }}
                            <div class="col-md-8">
                                <input type="hidden" name="command" value="pay">
                            </div>
                            <div class="col-md-8">
                                <label for="number">Номер кошелка</label>
                                <input type="tel" name="number" class="form-control">
                            </div>
                            <div class="col-md-8">
                                <label for="cash">Сумма</label>
                                <input type="number" name="amount" class="form-control">
                                <button type="submit" class="btn btn-primary pull-right">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
