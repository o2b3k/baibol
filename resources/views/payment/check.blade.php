@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Check</div>

                    <div class="panel-body">
                        <form action="{{ route('payment.Number') }}" method="get">
                            {{ csrf_field() }}
                            <input type="hidden" name="command" value="check">
                            <div class="col-md-8">
                                <label for="number">Номер кошелка</label>
                                <input type="tel" name="account" class="form-control">
                                <button type="submit" class="btn btn-primary pull-right">Проверить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
