<div class="dialogWin logoCrop">
	<h1>Выбрать область</h1>
	<div class="clear"></div>
	<div class="imageToCrop">
		<img src="/images/logo/[_logoUrl]" />
	</div>
	<div class="preview">
		<div class="avatar big"></div>
		<div class="avatar middle"></div>
		<div class="avatar small"></div>
	</div>
	<input type="hidden" name="x" value="" />
	<input type="hidden" name="y" value="" />
	<input type="hidden" name="w" value="" />
	<input type="hidden" name="h" value="" />
	<div class="clear"></div>
	<div class="footer">
		<button class="cropDone">Обрезать</button>
		<button class="cancel" data-fancybox-close="">Отмена</button>
	</div>
</div>
<script>
$(function() {
	crop();
	//$('.crop').on('click', function(){
	function crop(){
		$('.imageToCrop img').cropper({
			aspectRatio: 1 / 1,
			viewMode: 2,
			autoCropArea: 1,
			//movable: false,
			zoomable: false,
			preview: $('.preview .avatar'),
			//rotatable: false,
			//scalable: false
			crop: function(e) {
				// Output the result data for cropping image.
				$('.logoCrop input[name="x"]').val(e.x);
				$('.logoCrop input[name="y"]').val(e.y);
				$('.logoCrop input[name="w"]').val(e.width);
				$('.logoCrop input[name="h"]').val(e.height);
				//console.log(e.x);
				//console.log(e.y);
				//console.log(e.width);
				//console.log(e.height);
				//console.log(e.rotate);
				//console.log(e.scaleX);
				//console.log(e.scaleY);
			}
		});
	}
	//});

});
</script>