<div class="dialogWin delProfile">
	<h1>Удаление профиля</h1>
	<p>Удаление профиля приведет к удалению фото и других материалов, которые Вы размещали.</p>
	<p align="center" style="margin-bottom: 2em; font-size: 12pt;"><b>Вы уверены?</b></p>
	<div class="footer">
		<button class="delete">Удалить</button>
		<button class="cancel" data-fancybox-close="">Отмена</button>
	</div>
</div>