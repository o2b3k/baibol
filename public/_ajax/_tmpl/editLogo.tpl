<div class="dialogWin editLogoData">
	<h1>Изменить логотип</h1>
	<form id="upload_logo">
		<input type="hidden" name="id" value="[_id]" />
		<label>Выбрать фотографию</label>
		<input type="file" name="logo" id="attachment_image" accept="image/*"/>
	</form>
	<div class="logo">[_logo]</div>
	<input name="logoUrl" type="hidden" value="[_logoUrl]" />
	<div class="footer">
		<button class="save">Сохранить</button>
		<button class="cancel" data-fancybox-close="">Отмена</button>
	</div>
</div>