<div class="dialogWin editGalleryData">
	<h1>Галерея</h1>
	<form id="upload_logo">
		<input type="hidden" name="id" value="[_id]" />
		<label>Выбрать фотографию</label>
		<input type="file" name="logo" id="attachment_image" accept="image/*"/>
	</form>
	<div class="footer">
		<button class="save">Сохранить</button>
		<button class="cancel" data-fancybox-close="">Отмена</button>
	</div>
</div>