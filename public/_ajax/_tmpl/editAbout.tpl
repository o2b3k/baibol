<div class="dialogWin editAboutData">
	<h1>Описание компании</h1>
	<form>
		<label>Полное описание о компании</label>
		<textarea name="about" id="abouttext" style="" placeholder="Напишите что-нибудь...">[_about]</textarea>
	</form>
	<div class="footer">
		<button class="save">Сохранить</button>
		<button class="cancel" data-fancybox-close>Отмена</button>
	</div>
</div>
<script>
$(function() {
	tinymce.init({
		selector: '#abouttext',
		language: 'ru',
		skin: 'light',
		menubar: false,
		branding: false,
		statusbar: false,
		plugins: [
			'paste',
		],
		toolbar: 'undo redo header bold italic underline',
		content_css: ['/styles/textarea.css'],
		paste_data_images: false,
		paste_as_text: true,
		force_br_newlines : true,
		force_p_newlines : false,
		forced_root_block : ''
	});
});
</script>