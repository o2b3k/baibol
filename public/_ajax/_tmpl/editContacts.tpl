<div class="dialogWin editContactsData">
	<h1>Изменить адрес и контакты</h1>
	<form>
		<input type="hidden" name="formName" value="changeData">
		<label>Город</label>
		<select name="city" id="inputCity">
			<option value="1">Бишкек</option>
			<option value="2">Ош</option>
		</select>
		<label>Адрес</label>
		<input type="text" name="address" value="[_address]" placeholder="Пример: ул. Советская, 100" />
		<label>Телефон</label>
		<input type="text" name="phone" value="[_phone]" placeholder="Пример: 0555XXXXXX" />
		<label>Сайт</label>
		<input type="text" name="website" value="[_website]" placeholder="Пример: www.site.kg">
	</form>
	<div class="footer">
		<button class="save">Сохранить</button>
		<button class="cancel" data-fancybox-close>Отмена</button>
	</div>
</div>
<script>
$(function() {
	$('#inputCity').selectize();
});
</script>