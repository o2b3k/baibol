<script src="https://acquiring.mobilnik.kg/static/js/acquiring.js"></script>

<div class="dialogWin">
	<div class="top">
		<h1>Офомление заказа</h1>
		<div class="logo"></div>
	</div>
	<div class="bottom">
		<div class="row">
			<div class="col-8">
				<h3>Пожалуйста, заполните необходимые поля для заказа</h3>
				<form>
					<div class="row">
						<div class="col-4">
							<label>Заказ<span class="important">*</span></label>
						</div>
						<div class="col-8">
							<select id="food">
								<option name="food" value="1">Целый "Таш-Кордо"</option>
								<option name="food" value="2">Пол "Таш-Кордо"</option>
							</select>
						</div>
					</div>
					<div class="row food_1">
						<div class="col-4">&nbsp;</div>
						<div class="col-8">
							<b>Ваш заказ включает:</b>
							<ul>
								<li>Таш кордо из целого ягненка</li>
								<li>1.5 кг. жаренной картошки</li>
								<li>1.5 кг. овощей на гриле</li>
								<li>200 гр. соуса "Тартар"</li>
								<li>200 гр. соуса "Барбекю"</li>
							</ul>
						</div>
					</div>
					<div class="row food_2">
						<div class="col-4">&nbsp;</div>
						<div class="col-8">
							<b>Ваш заказ включает:</b>
							<ul>
								<li>Таш кордо из пол-ягненка</li>
								<li>0.5 кг. жаренной картошки</li>
								<li>0.5 кг. овощей на гриле</li>
								<li>100 гр. соуса "Тартар"</li>
								<li>100 гр. соуса "Барбекю"</li>
							</ul>
						</div>
					</div>
					<div class="col-4">
						<label>Вас зовут<span class="important">*</span></label>
					</div>
					<div class="col-8">
						<input type="text" name="name" placeholder="Имя" />
					</div>
					<div class="col-4">
						<label>Email<span class="important">*</span></label>
					</div>
					<div class="col-8">
						<input type="text" name="email" placeholder="Email" />
					</div>
					<div class="col-4">
						<label>Телефон<span class="important">*</span></label>
					</div>
					<div class="col-8">
						<input type="text" name="phone" placeholder="Номер телефона" />
					</div>
					<div class="col-4">
						<label>Адрес доставки<span class="important">*</span></label>
					</div>
					<div class="col-8">
						<input type="text" name="address" placeholder="Адрес" />
					</div>
					<div class="col-4">
						<label>Время доставки<span class="important">*</span></label>
					</div>
					<div class="col-8">
						<div class="col-5 datarange" style="padding-right: 2em; position: relative;">
							<input type="text" name="date" readonly />
						</div>
						<div class="col-3" style="padding-right: 0.5em;">
							<select id="time">
								<option name="time" value="1000">10</option>
								<option name="time" value="1100">11</option>
								<option name="time" value="1200">12</option>
								<option name="time" value="1300">13</option>
								<option name="time" value="1400">14</option>
								<option name="time" value="1500">15</option>
								<option name="time" value="1600">16</option>
								<option name="time" value="1700">17</option>
								<option name="time" value="1800">18</option>
								<option name="time" value="1900">19</option>
								<option name="time" value="2000">20</option>
								<option name="time" value="2100">21</option>
								<option name="time" value="2200">22</option>
							</select>
						</div>
						<div class="col-1" style="padding-right: 0; line-height: 70px; text-align: center;">
							<b>:</b>
						</div>
						<div class="col-3" style="padding-right: 0em;">
							<select id="time">
								<option name="time" value="1000">00</option>
								<option name="time" value="1100">15</option>
								<option name="time" value="1200">30</option>
								<option name="time" value="1300">45</option>
							</select>
						</div>
					</div>
					<br>
					<br>
					<!-- <input type="text" name="mobilnik_jwt_value" value="[_mobilnik_Jwt_value]" readonly /> -->
					<!-- <input type="text" name="mobilnik_key" value="[_mobilnik_Key]" readonly /> -->
					<br>
					<br>
					<div class="col-4">
						<label>Комментарии и пожелания</div></label>
					<div class="col-8"><textarea placeholder="комментарии"></textarea></div>
				</form>
			</div>
			<div class="col-4">
				<h3>Выберите способ оплаты</h3>
				<!-- <div class="pay_method"> -->
					<!-- <input type="radio" id="cash" name="pay_method"/>  -->
					<!-- <label for="cash">Наличными</label> -->
					<!-- <input type="radio" id="online" name="pay_method" checked="checked" />  -->
					<!-- <label for="online">Онлайн</label> -->
				<!-- </div> -->
				<!-- <p>Для завершения заказа путем депозита наличными, мы ждем вас с предоплатой за сутки до времени осуществелния доставки по нашему адресу: Мадиева, 18, с. Кок-Жар (ориентир Алматинка-Магистраль) (+карта)</p> -->
				<div class="payments">
					<!-- <h3>Выберите оператора:</h3> -->
					<!-- <p>или</p> -->
					<input type="radio" id="cash" name="payment" value="cash" checked="checked" /> 
					<label for="cash" class="payment cash"><span class="icon-attach_money" style="font-size: 24px; line-height: 32px;"></span></label>
					<input type="radio" id="visa" name="payment" value="visa" />
					<label for="visa" class="payment" style="background-position: -200px 0px;"></label>
					<input type="radio" id="mastercard" name="payment" value="mastercard" />
					<label for="mastercard" class="payment" style="background-position: 0px -100px"></label>
					<input type="radio" id="elsom" name="payment" value="elsom" />
					<label for="elsom" class="payment" style="background-position: 0px 0px"></label>
					<input type="radio" id="mobilnik" name="payment" value="mobilnik" />
					<label for="mobilnik" class="payment" style="background-position: -100px -100px"></label>
				</div>
				<hr />
				<h3>Доставка: бесплатно</h3>
				<h2>Итого: <span id="discount">12000 сом</span><span id="sum">9500</span> сом</h2>
				<hr />
				<div class="row">
					<div class="visa_description">
						<h3>Оплата картами</h3>
						<p style="font-size: 11pt;">Система автоматически перенаправит Вас на страницу оплаты где Вам необходимо будет заполнить данные своей банковской карты. Наша компания гарантирует полную безопасность денежных средств и персональных данных, так как введенные Вами реквизиты банковской карты абсолютно конфиденциальны. Вся информация передается по зашифрованным каналам.</p>
					</div>
					<!-- <p class="mc_description"><b>Оплата картой MasterCard</b></p> -->
					<div class="elsom_description">
						<h3>Оплата с помощью мобильного кошелька ЭЛСОМ</h3>
						<div style="padding: .75em 1em; background: green; color: #fff; text-align: center;">
							<b>09746</b>
							<div style="margin-top: 0.5em; font-size: 8pt; text-transform: uppercase;">Код продавца</div>
						</div>
						<p style="font-size: 11pt;"><!--Платить с помощью кошелька "ЭЛСОМ" также быстро и легко, как наличными, при этом, даже не нужно приходить в магазин. Все, что для этого нужно, это ввести 5-ти значный код магазина, сумму покупки и подвердить ПИН кодом.-->
						Платить с помощью кошелька "ЭЛСОМ" также быстро и легко, как наличными, при этом, даже не нужно приходить в магазин. Все, что для этого нужно, это перевести сумму покупки на указанный выше КОД ПРОДАВЦА.
						</p>
					</div>
					<div class="mobilnik_description">
						<h3>Оплата через Мобильник</h3>
						<p style="font-size: 11pt;">Вы получите SMS уведомление об оплате</p>
					</div>
				</div>
				<div class="cash_description">
					<h3>Оплата наличными</h3>
					<p style="font-size: 11pt;">Для завершения заказа путем депозита наличными, мы ждем вас с предоплатой за сутки до времени осуществелния доставки по нашему адресу: Мадиева, 18, с. Кок-Жар (ориентир Алматинка-Магистраль).</p>
				</div>
				<!-- <div class="row"><div class="col-8">&nbsp;</div><div class="col-4"><button class="btn" onclick="return false;">Заказать</button></div></div> -->
				<p style="color: red;" class="pay_info"><b></b></p>
				<div class="row"><div class="col-12" style="padding: 0;"><button class="btn issue" payMethod="cash" style="width: 100%;">Заказать звонок</button></div></div>
				<p>
					<i style="margin: 1em 0; font-size: 80%; line-height: 10px;">Для большей информации мы доступны по телефону +996 312 888008 или закажите обратный звонок и мы вам перезвоним.<br>
					(+карта)<br></i>
				</p>
			</div>
		</div>
	</div>
</div>
<script>
$(function(){
// change  ****************************************************** //
// ************************************************************** //
	
$('body [name="date"]').daterangepicker({
    singleDatePicker: true,
	locale: {
		format: 'DD-MM-YYYY',
		daysOfWeek: [
            "Вс",
            "Пн",
            "Вт",
            "Ср",
            "Чт",
            "Пт",
            "Сб"
        ],
		firstDay: 1,
        monthNames: [
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
        ],
    },
    "minDate": "[_minDate]", //Начало даты заказа
    "maxDate": "[_maxDate]", //Конец даты заказа
    "opens": "right",
	"drops": "up",
	"parentEl": '.datarange'
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});
	
});
</script>