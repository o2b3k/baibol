<div class="dialogWin changeData">
	<h1>Редакция данных</h1>
	<form>
		<input type="hidden" name="formName" value="changeData">
		<label>Категория "[_cat]"</label>
		<select id="inputCat" name="cats" multiple require>[_categories_select]</select>
		<label>Название <span class="require">*</span></label>
		<input type="text" name="name" value="[_name]" placeholder="введите название..." />
		<label>Краткое описание (155 символов)</label>
		<textarea name="description" placeholder="введите описание (необязательно)" maxlength="155">[_description]</textarea>
	</form>
	<div class="footer">
		<button class="save">Сохранить</button>
		<button class="cancel" data-fancybox-close>Отмена</button>
	</div>
</div>
<script>
$(function() {
	$('#inputCat').selectize({
		maxItems: 3
	});
});
</script>