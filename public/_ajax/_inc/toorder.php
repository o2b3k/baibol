<?
include_once '../mob/generate_token.php';

$content = file('_tmpl/toorder.tpl');
$content = implode($content);

$minDate = date('d-m-Y', strtotime('+1 day'));
$maxDate = date('d-m-Y', strtotime('+1 month'));

$content = str_replace('[_minDate]', $minDate, $content);
$content = str_replace('[_maxDate]', $maxDate, $content);

$content = str_replace('[_mobilnik_Jwt_value]', $jwtToken, $content);
$content = str_replace('[_mobilnik_Key]', $sellerIdentifier, $content);

$content = str_replace('[_order]', $sellerIdentifier, $content);

echo $content;
?>