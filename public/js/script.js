$(function(){
// change Name ************************************************** //
// ************************************************************** //
	
	$('#more').on('click',function(){
		var src = '/_ajax/conditions/';
		data = {id : ''};
		dialogWin(data, src);
	})
	
	$('.btn.toorder, #to_order').on('click', function(){
		// console.log('click');
		var src = '/_ajax/toorder/';
		data = {id : ''};
		dialogWin(data, src);
	});
	// $('body').on('click', '.changeData .save', function(){
		// var id = $('.content').attr('data-id'),
			// cats = $('.changeData [name="cats"]').val(),
			// name = $('.changeData [name="name"]').val(),
			// description = $('.changeData [name="description"]').val();
		// data = {id : id, cats : cats, name : name, description : description, save : 'save'};
		// $.ajax({
			// type: "POST",
			// cache: false,
			// url: '/_ajax/editPlace/',
			// data: data,
			// success: function (data) {
				// console.log(data);
				// location.reload();
			// }
		// });
	// });
	// ********************************************************** END //
	
	function dialogWin(data,src){
		//src = '/asd.php';
		$.ajax({
			type: "POST",
			cache: false,
			url: src,
			data: data, // your were using $(form).serialize(), 
			success: function (data) {
				fancyOpen(data);
			}
		});
	}
	function fancyOpen(data){
		$.fancybox.open(data, {
			smallBtn   : false,
			touch : false,
			buttons    : false,
			keyboard   : true,
			loop : false,
			beforeLoad: function(){
			},
			afterClose: function(){			
			},
			focus: false,
			animationEffect   : true,
		});
	}

	// mobilnik.kg *****************************
    function mobilnikBuy(jwt_value,key) {
		mobilnik.buy({
			jwt: jwt_value,
			key: key,
			success: function(e){
				console.log(e);
				$('body .dialogWin .pay_info b').html(e + '1');
			},
			failure: function(e) {
				console.log(e);
				$('body .dialogWin .pay_info b').html(e + '2');
			}
		});
		return false;
	}
	
	
	$('body').on('click', 'button.issue', function(){
		
		//var mobilnik_jwt_value = $('.dialogWin [name=mobilnik_jwt_value]').val(),
		//	mobilnik_key = $('.dialogWin [name=mobilnik_key]').val();
		
		//console.log(mobilnik_jwt_value);
		//console.log(mobilnik_key);
		
		//mobilnikBuy(mobilnik_jwt_value, mobilnik_key);
		
		//return false;
		
		var elName = $('.dialogWin [name="name"]');
		var name = elName.val();
		name = name.replace(/^\s+|\s+$/g, '')
		if(name == '') {
			elName.addClass('important');
		} else {
			elName.removeClass('important');
		}
		
		var elEmail = $('.dialogWin [name="email"]');
		var email = elEmail.val();
		email = email.replace(/^\s+|\s+$/g, '')
		if(email == '') {
			elEmail.addClass('important');
		} else {
			elEmail.removeClass('important');
		}
		
		var elPhone = $('.dialogWin [name="phone"]');
		var phone = elPhone.val();
		phone = phone.replace(/^\s+|\s+$/g, '')
		if(phone == '') {
			elPhone.addClass('important');
		} else {
			elPhone.removeClass('important');
		}
		
		var elAddress = $('.dialogWin [name="address"]');
		var address = elAddress.val();
		address = address.replace(/^\s+|\s+$/g, '')
		if(address == '') {
			elAddress.addClass('important');
		} else {
			elAddress.removeClass('important');
		}

		//var src = '/_ajax/confirm_order/';
		//data = {id : ''};
		//dialogWin(data, src);

		// elsom ******************
		// ************************
		var payMethod = $('body button.issue').attr('payMethod');
		if(payMethod == 'elsom'){
			window.open('http://www.elsom.kg','_blank');
			return false;
		}
		
		console.log(name);
		return false;
	});

	$('body').on('change', '.dialogWin .payments', function(){
		var selected = $(".dialogWin input[name=payment]:checked").val();
		//console.log(selected);
		if(selected == 'cash') {
			$('.cash_description').css('display','block');
			$('.visa_description').css('display','none');
			$('.mc_description').css('display','none');
			$('.elsom_description').css('display','none');
			$('.mobilnik_description').css('display','none');
			$('.issue').html('Заказать звонок');
			$('.issue').attr('payMethod','cash');
		} else if(selected == 'visa') {
			$('.cash_description').css('display','none');
			$('.visa_description').css('display','block');
			$('.mc_description').css('display','none');
			$('.elsom_description').css('display','none');
			$('.mobilnik_description').css('display','none');
			$('.issue').css('display','block');
			$('.issue').html('Оплатить');
			$('.issue').attr('payMethod','visa');
		} else if(selected == 'mastercard') {
			$('.cash_description').css('display','none');
			$('.visa_description').css('display','none');
			$('.mc_description').css('display','block');
			$('.elsom_description').css('display','none');
			$('.mobilnik_description').css('display','none');
			$('.issue').css('display','block');
			$('.issue').html('Оплатить');
			$('.issue').attr('payMethod','mastercard');
		} else if(selected == 'elsom') {
			$('.cash_description').css('display','none');
			$('.visa_description').css('display','none');
			$('.mc_description').css('display','none');
			$('.elsom_description').css('display','block');
			$('.mobilnik_description').css('display','none');
			$('.issue').css('display','none');
			$('.issue').attr('payMethod','elsom');
		} else {
			$('.cash_description').css('display','none');
			$('.visa_description').css('display','none');
			$('.mc_description').css('display','none');
			$('.elsom_description').css('display','none');
			$('.mobilnik_description').css('display','block');
			$('.issue').css('display','block');
			$('.issue').html('Оплатить');
			$('.issue').attr('payMethod','mobilnik');
		}
	});
	
	$('body').on('change', '.dialogWin #food', function(){
		var selected = $(".dialogWin #food").val();
		// console.log(selected);
		if(selected == 1) {
			$('.food_1').css('display','block');
			$('.food_2').css('display','none');
			$('#discount').html('12000 сом');
			$('#sum').html('9500');
		} else {
			$('.food_1').css('display','none');
			$('.food_2').css('display','block');
			$('#discount').html('8000 сом');
			$('#sum').html('6000');
		}
	});
	
	
	
});