<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index');
Route::get('/test', 'FrontendController@getRequest');
Route::get('/payment', 'MobileController@index')->name('payment.Index');
Route::get('/payment/check', 'MobileController@check')->name('payment.Number');
Route::get('/payment/payment', 'MobileController@pay')->name('payment.Pay');
Route::get('/payment/pay', 'MobileController@payment')->name('payment.Payment');

Route::get('/xml', function () {
    $data = "user";
    return response()->xml($data);
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
