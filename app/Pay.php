<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pay extends Model
{
    protected $table = 'pays';

    protected $fillable = ['command', 'account','txn_id', 'txn_date', 'amount'];
}
