<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
class FrontendController extends Controller
{
    var $elSom = 'https://212.112.122.221';
    var $kz = 'https://212.154.215.82';

    public function index()
    {
        return view('layouts.master');
    }

    public function dataJson()
    {
        $data = array(
            "PartnerGenerateOTP" => array(
                "CultureInfo" => "ru-Ru",
                "MSISDN" => "0555150155",
                "PartnerCode" => "00609",
                "ChequeNo" => "123456",
                "PartnerTrnID" => "123456",
                "Amount" => "100.50",
                "CashierNo" => "123",
                "UDF" => "TEST",
                "Password" => "Password1"
            ),
        );
        return $data;
    }

    public function getRequest(){
        $response = Curl::to($this->elSom)
            ->withContentType('text/json')
            ->withData($this->dataJson())
            ->post();
        dd($response);
    }
}
