<?php

namespace App\Http\Controllers;

use App\Member;
use App\Pay;
use Illuminate\Http\Request;
use Validator;

class MobileController extends Controller
{
    public function index()
    {
        return view('payment.check');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function check(Request $request)
    {
        $number = $request->input('account');
        $command = $request->input('command');
        $member = Member::where('account',$number)->first();
        if (!$member){
            $dom2 = new \DOMDocument('1.0','utf-8');
            $response2 = $dom2->appendChild($dom2->createElement('response'));
            $res = $response2->appendChild($dom2->createElement('result'));
            $com = $response2->appendChild($dom2->createElement('comment'));
            $res->appendChild($dom2->createTextNode('1'));
            $com->appendChild($dom2->createTextNode('нет такой номер'));
            $dom2->formatOutput = true;
            $dom2->preserveWhiteSpace = false;
            $test2 = $dom2->saveXML();
            echo $test2;
        }
        if ($member){
            if ($command !== "check"){
                return response()->json([
                    'error' => 'Error command method',
                ]);
            }
            $generate_prv_txn = $member->account;
            $generate_result = 0;
            $generate_comment = $member->name;
            //Создает XML-строку и XML-документ при помощи DOM
            $dom = new \DOMDocument('1.0','utf-8');
            $response = $dom->appendChild($dom->createElement('response'));
            $prv_txn = $response->appendChild($dom->createElement('prv_txn'));
            $result = $response->appendChild($dom->createElement('result'));
            $comment = $response->appendChild($dom->createElement('comment'));
            $prv_txn->appendChild($dom->createTextNode($generate_prv_txn));
            $result->appendChild($dom->createTextNode($generate_result));
            $comment->appendChild($dom->createTextNode($generate_comment));
            $dom->formatOutput = true;
            $dom->preserveWhiteSpace = false;
            $test1 = $dom->saveXML();
            echo $test1;
        }
    }

    public function memberRules()
    {
        return [
            'account' => 'required|unique:members',
        ];
    }

    public function pay()
    {
        return view('payment.payment');
    }

    public function payment(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules());
        if ($validator->fails()){
            $dom2 = new \DOMDocument('1.0','utf-8');
            $response2 = $dom2->appendChild($dom2->createElement('response'));
            $res = $response2->appendChild($dom2->createElement('result'));
            $com = $response2->appendChild($dom2->createElement('comment'));
            $res->appendChild($dom2->createTextNode('07'));
            $com->appendChild($dom2->createTextNode('есть такой платеж с таким идентификатором'));
            $dom2->formatOutput = true;
            $dom2->preserveWhiteSpace = false;
            $test2 = $dom2->saveXML();
            echo $test2;
        }
        if (!$validator->fails()){
            $command = $request->input('command');
            if ($command !== "pay"){
                return response()->json([
                    'error' => 'Error command method',
                ]);
            }
            $account = $request->input('account');
            if ($account == ""){
                return response()->json([
                    'error' => 'Enter a account number',
                ]);
            }
            $txn_id = $request->input('txn_id');
            $txn_date = $request->input('txn_date');
            $cash = $request->input('amount');

            $pay = new Pay();
            $pay->command = $command;
            $pay->account = $account;
            $pay->txn_id = $txn_id;
            $pay->txn_date = $txn_date;
            $pay->amount = $cash;
            $pay->save();

            $generate_osmp_txn_id = $txn_id;
            $generate_prv_txn = random_int(1000000,9999999);
            $generate_sum = $cash;
            $generate_result = 0;
            $generate_comment = "Ok";

            //Создает XML-строку и XML-документ при помощи DOM
            $dom = new \DOMDocument('1.0','utf-8');
            $response = $dom->appendChild($dom->createElement('response'));
            $osmp_txn_id = $response->appendChild($dom->createElement('osmp_txn_id'));
            $prv_txn = $response->appendChild($dom->createElement('prv_txn'));
            $sum = $response->appendChild($dom->createElement('sum'));
            $result = $response->appendChild($dom->createElement('result'));
            $comment = $response->appendChild($dom->createElement('comment'));
            $osmp_txn_id->appendChild($dom->createTextNode($generate_osmp_txn_id));
            $prv_txn->appendChild($dom->createTextNode($generate_prv_txn));
            $sum->appendChild($dom->createTextNode($generate_sum));
            $result->appendChild($dom->createTextNode($generate_result));
            $comment->appendChild($dom->createTextNode($generate_comment));
            $dom->formatOutput = true;
            $dom->preserveWhiteSpace = false;
            $payment = $dom->saveXML();
            echo $payment;
        }
    }

    public function rules()
    {
        return [
            'txn_id' => 'required|unique:pays',
        ];
    }

    public function messages()
    {
        return [
            'txn_id' => 'есть такой платеж с таким идентификатором',
        ];
    }

}
